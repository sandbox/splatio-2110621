<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Form\FormatConfigurationForm
 */

namespace Drupal\hardcopy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\hardcopy\HardcopyFormatPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a configuration form for a given hardcopy format plugin.
 */
class FormatConfigurationForm extends FormBase {

  /**
   * The hardcopy format plugin manager.
   *
   * @var \Drupal\hardcopy\HardcopyFormatPluginManager
   */
  protected $hardcopyFormatManager;

  /**
   * The plugin instance being configured.
   *
   * @var \Drupal\hardcopy\Plugin\HardcopyFormatInterface;
   */
  protected $plugin;

  /**
   * Constructs a new form object.
   *
   * @param \Drupal\hardcopy\HardcopyFormatPluginManager $hardcopy_manager
   *   The hardcopy format plugin manager.
   */
  public function __construct(HardcopyFormatPluginManager $hardcopy_manager) {
    $this->hardcopyFormatManager = $hardcopy_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.hardcopyformat')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'hardcopy_format_configuration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hardcopy_format = NULL) {
    if ($this->hardcopyFormatManager->getDefinition($hardcopy_format)) {
      $this->plugin = $this->hardcopyFormatManager->createInstance($hardcopy_format);
      $form += $this->plugin->buildConfigurationForm($form, $form_state);

      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
      );

      return $form;
    }
    else {
      throw new NotFoundHttpException;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->submitConfigurationForm($form, $form_state);
  }
}
