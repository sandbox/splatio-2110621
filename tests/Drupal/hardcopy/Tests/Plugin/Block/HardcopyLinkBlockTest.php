<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Tests\Plugin\Block\HardcopyLinksBlockTest
 */

namespace Drupal\hardcopy\Tests\Plugin\Block;

use Drupal\Tests\UnitTestCase;
use Drupal\hardcopy\Plugin\Block\HardcopyLinksBlock;

/**
 * Tests the hardcopy links block plugin.
 *
 * @group Hardcopy
 */
class HardcopyLinksBlockTest extends UnitTestCase {

  protected $configuration = array();

  protected $pluginId;

  protected $pluginDefinition = array();

  public function __construct() {
    parent::__construct();
    $this->pluginId = 'hardcopy_links_block:foo';
    $this->pluginDefinition['module'] = 'hardcopy';
  }

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Hardcopy Block',
      'descriptions' => 'Tests the hardcopy block plugin class.',
      'group' => 'Hardcopy',
    );
  }

  /**
   * Tests the block build method.
   */
  public function testBuild() {
    $request = $this->getMockBuilder('Symfony\Component\HttpFoundation\Request')
      ->disableOriginalConstructor()
      ->getMock();
    $request->attributes = $this->getMock('Symfony\Component\HttpFoundation\ParameterBag');
    $request->attributes->expects($this->once())
      ->method('has')
      ->will($this->returnValue(TRUE));
    $request->attributes->expects($this->once())
      ->method('get')
      ->will($this->returnValue($this->getMock('Drupal\Core\Entity\EntityInterface')));

    $links = array(
      'title' => 'Print',
      'href' => '/foo/1/hardcopy/print',
      'attributes' => array(
        'target' => '_blank',
      ),
    );
    $links_builder = $this->getMockBuilder('Drupal\hardcopy\HardcopyLinkBuilderInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $links_builder->expects($this->once())
      ->method('buildLinks')
      ->will($this->returnValue($links));

    $block = new HardcopyLinksBlock($this->configuration, $this->pluginId, $this->pluginDefinition, $request, $links_builder);

    $expected_build = array(
      '#theme' => 'links__entity__hardcopy',
      '#links' => $links,
    );
    $this->assertEquals($expected_build, $block->build());
  }
}
