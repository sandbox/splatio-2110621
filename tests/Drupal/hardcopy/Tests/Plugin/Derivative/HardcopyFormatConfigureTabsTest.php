<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Tests\Plugin\Derivative\HardcopyFormatConfigureTabsTest
 */

namespace Drupal\hardcopy\Tests\Plugin\Derivative;

use Drupal\Tests\UnitTestCase;
use Drupal\hardcopy\Plugin\Derivative\HardcopyFormatConfigureTabs;

/**
 * Tests the hardcopy configuration tabs plugin derivative.
 *
 * @group Hardcopy
 */
class HardcopyFormatConfigureTabsTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Hardcopy Tabs Plugin Derivative',
      'descriptions' => 'Tests the hardcopy tabs plugin derivative class.',
      'group' => 'Hardcopy',
    );
  }

  /**
   * Tests getting the plugin label from the plugin.
   */
  public function testGetDerivativeDefinitions() {
    $hardcopy_format_manager = $this->getMockBuilder('Drupal\hardcopy\HardcopyFormatPluginManager')
      ->disableOriginalConstructor()
      ->getMock();
    $hardcopy_format_manager->expects($this->once())
      ->method('getDefinitions')
      ->will($this->returnValue(array(
        'foo' => array(
          'title' => 'Foo',
        ),
        'bar' => array(
          'title' => 'Bar',
        ),
      )));
    $derivative = new HardcopyFormatConfigureTabs($hardcopy_format_manager);

    $expected = array(
      'foo' => array(
        'title' => 'Foo',
        'route_parameters' => array('hardcopy_format' => 'foo'),
      ),
      'bar' => array(
        'title' => 'Bar',
        'route_parameters' => array('hardcopy_format' => 'bar'),
      ),
    );
    $this->assertEquals($expected, $derivative->getDerivativeDefinitions(array()));
  }
}
