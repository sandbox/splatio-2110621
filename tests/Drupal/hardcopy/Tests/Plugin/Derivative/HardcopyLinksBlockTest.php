<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Tests\Plugin\Derivative\HardcopyLinksBlockTest
 */

namespace Drupal\hardcopy\Tests\Plugin\Derivative;

use Drupal\Tests\UnitTestCase;
use Drupal\hardcopy\Plugin\Derivative\HardcopyLinksBlock;

/**
 * Tests the hardcopy links block plugin derivative..
 *
 * @group Hardcopy
 */
class HardcopyLinksBlockTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Hardcopy Block Derivative',
      'descriptions' => 'Tests the hardcopy block plugin derivative class.',
      'group' => 'Hardcopy',
    );
  }

  /**
   * Tests getting the plugin label from the plugin.
   */
  public function testGetDerivativeDefinitions() {
    $hardcopy_format_manager = $this->getMockBuilder('Drupal\hardcopy\HardcopyEntityManager')
      ->disableOriginalConstructor()
      ->getMock();
    $hardcopy_format_manager->expects($this->once())
      ->method('getHardcopyEntities')
      ->will($this->returnValue(array(
        'foo' => array(
          'label' => 'Foo',
        ),
        'bar' => array(
          'label' => 'Bar',
        ),
      )));
    $derivative = new HardcopyLinksBlock($hardcopy_format_manager);
    $base_plugin_definition = array(
      'admin_label' => 'Hardcopy Links Block'
    );

    $expected = array(
      'foo' => array(
        'admin_label' => 'Hardcopy Links Block (Foo)',
      ),
      'bar' => array(
        'admin_label' => 'Hardcopy Links Block (Bar)',
      ),
    );
    $this->assertEquals($expected, $derivative->getDerivativeDefinitions($base_plugin_definition));
  }
}
