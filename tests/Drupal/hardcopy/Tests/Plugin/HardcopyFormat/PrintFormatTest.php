<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Tests\Plugin\HardcopyFormat\PrintFormatTest
 */

namespace Drupal\hardcopy\Tests\Plugin\HardcopyFormat;

use Drupal\Tests\UnitTestCase;
use Drupal\hardcopy\Plugin\HardcopyFormat\PrintFormat;

/**
 * Tests the print format plugin.
 *
 * @group Hardcopy
 */
class PrintFormatTest extends UnitTestCase {

  /**
   * The plugin definition of this plugin.
   *
   * @var array
   */
  protected $pluginDefinition;

  /**
   * The ID of the plugin.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The configuration to be passed into the format plugin constructor.
   *
   * @var array
   */
  protected $configuration;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->pluginDefinition = array(
      'description' => 'Print description.',
      'id' => 'print',
      'module' => 'hardcopy',
      'title' => 'Print',
      'class' => 'Drupal\hardcopy\Plugin\HardcopyFormat\PrintFormat',
      'provider' => 'hardcopy',
    );
    $this->pluginId = 'print';
    $this->configuration = array();
  }

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Hardcopy Format Base',
      'descriptions' => 'Tests the hardcopy format base class.',
      'group' => 'Hardcopy'
    );
  }

  /**
   * Tests getting the plugin label from the plugin.
   */
  public function testGetLabel() {
    $format = new PrintFormat($this->configuration, $this->pluginId, $this->pluginDefinition, $this->getConfigFactoryStub(), $this->getCssIncludeStub());
    $this->assertEquals('Print', $format->getLabel());
  }

  /**
   * Tests getting the plugin description from the plugin.
   */
  public function testGetDescription() {
    $format = new PrintFormat($this->configuration, $this->pluginId, $this->pluginDefinition, $this->getConfigFactoryStub(), $this->getCssIncludeStub());
    $this->assertEquals('Print description.', $format->getDescription());
  }

  /**
   * Tests getting the default configuration for this plugin.
   */
  public function testDefaultConfiguration() {
    $format = new PrintFormat($this->configuration, $this->pluginId, $this->pluginDefinition, $this->getConfigFactoryStub(), $this->getCssIncludeStub());
    $this->assertEquals(array('show_print_dialogue' => TRUE), $format->defaultConfiguration());
  }

  /**
   * Tests getting the current configuration for this plugin.
   */
  public function testGetConfiguration() {
    $format = new PrintFormat($this->configuration, $this->pluginId, $this->pluginDefinition, $this->getConfigFactoryStub(), $this->getCssIncludeStub());
    $this->assertEquals(array('show_print_dialogue' => TRUE), $format->getConfiguration());
  }

  /**
   * Tests that any additional configuration passed is internally stored and can
   * be accessed.
   */
  public function testGetPassedInConfiguration() {
    $format = new PrintFormat(array('test_configuration_value' => TRUE), $this->pluginId, $this->pluginDefinition, $this->getConfigFactoryStub(), $this->getCssIncludeStub());
    $this->assertEquals(array('show_print_dialogue' => TRUE, 'test_configuration_value' => TRUE), $format->getConfiguration());
  }

  /**
   * Test that default configuration can be modified and changes accessed.
   */
  public function testSetConfiguration() {
    $new_configuration = array('show_print_dialogue' => FALSE);

    $config_mock = $this->getMockBuilder('\Drupal\Core\Config\Config')
      ->disableOriginalConstructor()
      ->getMock();
    $config_mock->expects($this->once())
      ->method('set')
      ->with('print', $new_configuration)
      ->will($this->returnSelf());
    $config_mock->expects($this->once())
      ->method('save');

    $config_factory_mock = $this->getMockBuilder('\Drupal\Core\Config\ConfigFactory')
      ->disableOriginalConstructor()
      ->getMock();
    $config_factory_mock->expects($this->once())
      ->method('get')
      ->with('hardcopy.format')
      ->will($this->returnValue($config_mock));

    $format = new PrintFormat($this->configuration, $this->pluginId, $this->pluginDefinition, $config_factory_mock, $this->getCssIncludeStub());
    $format->setConfiguration($new_configuration);

    $this->assertEquals($new_configuration, $format->getConfiguration());
  }

  /**
   * Get the CSS include stub
   */
  protected function getCssIncludeStub() {
    return $this->getMockBuilder('Drupal\hardcopy\HardcopyCssIncludeInterface')
      ->disableOriginalConstructor()
      ->getMock();
  }
}
