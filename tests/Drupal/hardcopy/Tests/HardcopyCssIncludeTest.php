<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Tests\HardcopyCssIncludeTest
 */

namespace Drupal\hardcopy\Tests;

use Drupal\Tests\UnitTestCase;
use Drupal\hardcopy\HardcopyCssInclude;

/**
 * Tests the print format plugin.
 *
 * @group Hardcopy
 */
class HardcopyCssIncludeTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Hardcopy CSS Include',
      'descriptions' => 'Tests the hardcopy CSS include class.',
      'group' => 'Hardcopy'
    );
  }

  /**
   * Tests getting the plugin label from the plugin.
   *
   * @dataProvider providerTestGetCssIncludePath
   */
  public function testGetCssIncludePath($include, $expected) {
    $config = $this->getConfigFactoryStub(array('hardcopy.settings' => array('css_include' => $include)));

    $theme_info = array(
      'bartik' => new \stdClass(),
    );
    $theme_info['bartik']->uri = 'core/themes/bartik/bartik.info.yml';
    $theme_handler = $this->getMockBuilder('Drupal\Core\Extension\ThemeHandlerInterface')
      ->disableOriginalConstructor()
      ->getMock();
    $theme_handler->expects($this->any())
      ->method('listInfo')
      ->will($this->returnValue($theme_info));

    $css_include = new HardcopyCssInclude($config, $theme_handler);

    $this->assertEquals($expected, $css_include->getCssIncludePath());
  }

  /**
   * Data provider for testGetCssIncludePath().
   */
  public function providerTestGetCssIncludePath() {
    return array(
      array('[theme:bartik]/css/test.css', 'core/themes/bartik/css/test.css'),
      array('[theme:foobar]/css/test.css', '/css/test.css'),
      array('foo/bar/css/test.css', 'foo/bar/css/test.css'),
    );
  }
}
