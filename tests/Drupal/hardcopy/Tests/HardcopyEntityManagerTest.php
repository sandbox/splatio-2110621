<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Tests\HardcopyEntityManagerTest
 */

namespace Drupal\hardcopy\Tests;

use Drupal\Tests\UnitTestCase;
use Drupal\hardcopy\HardcopyEntityManager;

/**
 * Tests the hardcopy entity manager plugin.
 *
 * @group Hardcopy
 */
class HardcopyEntityManagerTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Hardcopy Entity Manager',
      'descriptions' => 'Tests the hardcopy entity manager class.',
      'group' => 'Hardcopy'
    );
  }

  /**
   * Tests getting the hardcopy entities.
   */
  public function testGetHardcopyEntities() {
    // Construct a hardcopy entity manager and it's dependencies.
    $entity_manager = $this->getMock('Drupal\Core\Entity\EntityManagerInterface');
    $entity_manager->expects($this->once())
      ->method('getDefinitions')
      ->will($this->returnValue(array(
        'node' => array(
          'controllers' => array('view_builder' => NULL),
        ),
        'comment' => array(
          'controllers' => array('view_builder' => NULL),
        ),
        'foo' => array(
          'controllers' => array(),
        ),
      ))
    );
    $config = $this->getConfigFactoryStub(array(
      'hardcopy.settings' => array(
        'hardcopy_entities' => array('node', 'comment', 'bar'),
      ),
    ));
    $hardcopy_entity_manager = new HardcopyEntityManager($entity_manager, $config);

    // Verify getting the hardcopy entities.
    $expected_entity_definitions = array(
      'node' => array(
        'controllers' => array('view_builder' => NULL),
      ),
      'comment' => array(
        'controllers' => array('view_builder' => NULL),
      ),
    );
    $this->assertEquals($expected_entity_definitions, $hardcopy_entity_manager->getHardcopyEntities());
  }

  /**
   * Tests checking if entity is hardcopy entity.
   */
  public function testIsHardcopyEntity() {
    // Construct a hardcopy entity manager and it's dependencies.
    $entity_manager = $this->getMock('Drupal\Core\Entity\EntityManagerInterface');
    $entity_manager->expects($this->once())
      ->method('getDefinitions')
      ->will($this->returnValue(array(
        'node' => array(
          'controllers' => array('view_builder' => NULL),
        ),
      ))
    );
    $config = $this->getConfigFactoryStub(array(
      'hardcopy.settings' => array(
        'hardcopy_entities' => array('node'),
      ),
    ));
    $hardcopy_entity_manager = new HardcopyEntityManager($entity_manager, $config);

    // Create the entity mock.
    $entity = $this->getMock('Drupal\Core\Entity\EntityInterface');
    $entity->expects($this->any())
      ->method('entityType')
      ->will($this->onConsecutiveCalls('node', 'bar'));

    // Entity type is "node".
    $this->assertTrue($hardcopy_entity_manager->isHardcopyEntity($entity));
    // Entity type is "bar".
    $this->assertFalse($hardcopy_entity_manager->isHardcopyEntity($entity));
  }

  /**
   * Tests getting the hardcopy entities.
   */
  public function testGetCompatibleEntities() {
    // Construct a hardcopy entity manager and it's dependencies.
    $entity_manager = $this->getMock('Drupal\Core\Entity\EntityManagerInterface');
    $entity_manager->expects($this->once())
      ->method('getDefinitions')
      ->will($this->returnValue(array(
          'node' => array(
            'controllers' => array('view_builder' => NULL),
          ),
          'comment' => array(
            'controllers' => array('view_builder' => NULL),
          ),
          'foo' => array(
            'controllers' => array(),
          ),
        ))
      );
    $hardcopy_entity_manager = new HardcopyEntityManager($entity_manager, $this->getConfigFactoryStub());

    // Verify getting the hardcopy entities.
    $expected_entity_definitions = array(
      'node' => array(
        'controllers' => array('view_builder' => NULL),
      ),
      'comment' => array(
        'controllers' => array('view_builder' => NULL),
      ),
    );
    $this->assertEquals($expected_entity_definitions, $hardcopy_entity_manager->getCompatibleEntities());
  }
}
