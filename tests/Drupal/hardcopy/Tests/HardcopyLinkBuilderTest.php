<?php

/**
 * @file
 * Contains \Drupal\hardcopy\Tests\HardcopyLinkBuilderTest
 */

namespace Drupal\hardcopy\Tests;

use Drupal\Tests\UnitTestCase;
use Drupal\hardcopy\HardcopyLinkBuilder;

/**
 * Tests the print format plugin.
 *
 * @group Hardcopy
 */
class HardcopyLinkBuilderTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Hardcopy Link Builder',
      'descriptions' => 'Tests the hardcopy link builder class.',
      'group' => 'Hardcopy'
    );
  }

  /**
   * Tests generating the render array of hardcopy links.
   */
  public function testBuildLinks() {
    $definitions = array(
      'foo' => array(
        'title' => 'Foo',
      ),
      'bar' => array(
        'title' => 'Bar',
      ),
    );
    $entity_type = 'node';
    $entity_id = rand(1, 100);

    $config = $this->getConfigFactoryStub(array('hardcopy.settings' => array('open_target_blank' => TRUE)));

    $url_generator = $this->getMock('Drupal\Core\Routing\UrlGeneratorInterface');
    $url_generator->expects($this->exactly(2))
      ->method('generateFromRoute')
      ->with($this->logicalOr(
        'hardcopy.show_format.' . $entity_type, array('hardcopy_format' => 'foo', 'entity' => $entity_id),
        'hardcopy.show_format.' . $entity_type, array('hardcopy_format' => 'bar', 'entity' => $entity_id)
      ))
      ->will($this->returnValue('some/path'));

    $hardcopy_manager = $this->getMockBuilder('Drupal\hardcopy\HardcopyFormatPluginManager')
      ->disableOriginalConstructor()
      ->getMock();
    $hardcopy_manager->expects($this->once())
      ->method('getDefinitions')
      ->will($this->returnValue($definitions));

    $link_builder = new HardcopyLinkBuilder($config, $url_generator, $hardcopy_manager);

    $entity = $this->getMock('Drupal\Core\Entity\EntityInterface');
    $entity->expects($this->exactly(2))
      ->method('entityType')
      ->will($this->returnValue($entity_type));
    $entity->expects($this->exactly(2))
      ->method('id')
      ->will($this->returnValue($entity_id));

    $links = $link_builder->buildLinks($entity);
    $this->assertEquals(2, count($links));
    foreach($definitions as $key => $definition) {
      $link = $links[$key];
      $this->assertEquals($definition['title'], $link['title']);
      $this->assertEquals('some/path', $link['href']);
      $this->assertEquals('_blank', $link['attributes']['target']);
    }
  }
}
